// ==UserScript==
// @name         wieselschreck für laut.de
// @namespace    http://tampermonkey.net/
// @version      0.3
// @description  remove all comments by *wiesel* from laut.de
// @author       CAPSLOCKFTW
// @match        https://www.laut.de/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=laut.de
// @require      http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js
// @grant        GM_addStyle
// ==/UserScript==

(function() {
    'use strict';
    //remove all annoying wieselcontent
    var badKommis = $("div [title]:contains('wiesel'),div [title]:contains('w1esel')");
    var badMinime = $("[title*='wiesel'],[title*='w1esel']");
    badKommis.parent().parent().remove();
    badMinime.parent().remove();
})();
